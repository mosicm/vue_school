Vue.component('github-user-card', {
    props: {
        username: {
            type: String,
            required: true,
        }
    },
    template: `
        <div class="ui card">
            <div class="image">
               <img :src="user.avatar_url" alt="avatar">
            </div>
            <div class="content">
                <a :href="this.user.html_url" class="header">{{user.name}}</a>
                <div class="meta">
                    <span class="date">Joined in {{user.created_at}}</span>
                </div>
                <div class="description">
                    {{user.bio}}.
                </div>
            </div>
            <div class="extra content">
                <a :href="followersUrl">
                    <i class="user icon"></i>
                    {{user.followers}} Friends
                </a>
            </div>
        </div>
    `,
    data() {
        return {
            user: {}
        }
    },
    created() {
        axios.get(`https://api.github.com/users/${this.username}`)
            .then(response => {
                this.user = response.data
                console.log(response.data);
            })
    },
    computed: {
        followersUrl() {
            return this.user.html_url + '?tab=followers';
        },
    }
});

new Vue({
   el: '#app',
   data: {
       usernames: ['hootlex', 'rahaug', 'sdras', 'akryum'],
   }
});

