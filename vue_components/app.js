let ClickCounterComponent = {
    template: `<button class="btn btn-primary" @click="incrementCounter">You clicked me {{counter}} times</button>`,
    data: function () {
        return {
            counter: 0,
        }
    },
    methods: {
        incrementCounter: function () {
            this.counter++;
        }
    }
};

let MovieComponent = {
    template: `<div class="list-group-item" :class="{'active': isSelected}" @click="selectMovie">{{ name }}</div>`,
    props: ['name', 'selectedMovie'],
    methods: {
        selectMovie() {
            this.$emit('selected', this.name);
        }
    },
    computed: {
        isSelected() {
            return this.name === this.selectedMovie;
        }
    }
};

let MovieListComponent = {
    components: {
        'movie': MovieComponent,
    },
    template: `<div><movie v-for="name in names" :name="name" @selected="selectMovie" :selectedMovie="selectedMovie"></movie></div>`,
    data: function () {
        return {
            names: ['Take Shelter', 'Dallas Buyers Club', 'The Prisoners', 'Ex Machine'],
            selectedMovie: null,
        }
    },
    methods: {
        selectMovie(movie) {
            this.selectedMovie = movie;
        }
    }
};

new Vue({
    el: "#app",
    components: {
        'movie-list': MovieListComponent,
        'click-counter': ClickCounterComponent,
    },
});
