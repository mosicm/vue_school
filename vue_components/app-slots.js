Vue.component('page-layout', {
    template: `
        <div>
            <ul class="nav">
                <slot name="header"></slot>
            </ul>
            <slot></slot>
            <p class="row justify-content-md-center"><slot name="footer"></slot></p>
        </div>
    `,
});

new Vue({
    el: "#app",
});
