let app = new Vue({
	el: '#dream-team',
	data: {
		title: 'dream team of the 90\'s!',
		description: 'This is a list of the best basketball players in the 90\'s era.',
		players: [
			'Michael Jordan',
			'Charles Barkley',
			'Earvin Johnson',
		],
		newPlayer: '',
		addPlayers: false,
		buttonText: 'Add players',
	},
	methods: {
		savePlayer: function() {
			this.players.push(this.newPlayer);
			this.newPlayer = '';
		},
		toggleButton: function() {
			this.addPlayers = !this.addPlayers;
			this.buttonText = !this.addPlayers ? 'Add players' : 'Cancel';
		}
	},
    computed: {
        oldestPlayersFirst: function() {
	        return this.players.slice(0).reverse();
        }
    }
});
